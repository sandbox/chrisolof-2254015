<?php

/**
 * @file
 * Enables a map link field formatter for use with geofields.
 */

/**
 * Implements hook_field_formatter_info().
 */
function geofield_map_link_field_formatter_info() {
  return array(
    'geofield_map_link_link' => array(
      'label' => t('Geofield Map Link'),
      'field types' => array('geofield'),
      'settings' => array(
        'geofield_map_link_text' => t('Map'),
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function geofield_map_link_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();

  list($entity_id) = entity_extract_ids($entity_type, $entity);

  $settings = $display['settings'];

  foreach ($items as $item) {
    $element[] = array(
      '#theme' => 'link',
      '#text' => $display['settings']['geofield_map_link_text'],
      '#path' => 'https://maps.google.com/maps',
      '#options' => array(
        'html' => FALSE,
        'external' => TRUE,
        'query' => array(
          'q' => $item['lat'] . ',' . $item['lon'],
        ),
        'attributes' => array(
          'target' => '_blank'
        ),
      ),
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function geofield_map_link_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = geofield_map_link_settings_form($settings);

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function geofield_map_link_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = array();

  if ($settings['geofield_map_link_text']) {
    $summary[] = t('Link text: @text', array('@text' => $settings['geofield_map_link_text']));
  }

  return implode('<br />', $summary);
}

/**
 * Returns the settings form for this field formatter.
 */
function geofield_map_link_settings_form($settings, $element = array()) {
  $element['geofield_map_link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Link text'),
    '#default_value' => $settings['geofield_map_link_text'],
    '#required' => TRUE,
  );

  return $element;
}
